/**
 * 
 */
package org.parcc.cat.dcm.controller;

import java.util.List;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.parcc.cat.dcm.bean.Scores;
import org.parcc.cat.dcm.bean.Selection;
import org.parcc.cat.dcm.bean.StudentState;
import org.parcc.cat.dcm.service.IStudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author alex.filipovik
 *
 */
@RestController
public class StudentController {
	private static final Logger logger = LoggerFactory.getLogger(StudentController.class);
	
	@Autowired
	IStudentService studentService;
	
	@RequestMapping(value="/items/{examId}", method = RequestMethod.POST)
	public Selection getItems(
			@PathVariable String examId,
			@RequestBody Scores scores) 
	{
		try {
			Selection selection = studentService.updateScoreAndSelectItems(examId, scores);
			return selection;
		} catch(Exception ex) {
			logger.error(String.format("Failed to update scores and select items: examId=%s;", examId), ex);
			Selection selection = new Selection();
			selection.setError(ex.getMessage());
			return selection;
		}
	}
	
	@RequestMapping(value="/items/{examId}/{studentId}/{slot}", method = RequestMethod.GET)
	public Selection getItems(
			@PathVariable String examId, 
			@PathVariable String studentId, 
			@PathVariable int slot) 
	{
		try {
			Selection selection = studentService.getItems(examId, studentId, slot);
			return selection;
		} catch(Exception ex) {
			logger.error(String.format("Failed to get items: examId=%s; studentId=%s; slot=%d", examId, studentId, slot), ex);
			Selection selection = new Selection();
			selection.setError(ex.getMessage());
			return selection;
		}
	}
	
	@RequestMapping(value="/replace/{examId}/{studentId}/{slot}", method = RequestMethod.GET)
	public Selection replaceItems(
			@PathVariable String examId, 
			@PathVariable String studentId, 
			@PathVariable int slot) 
	{
		try {
			Selection selection = studentService.replaceItems(examId, studentId, slot);
			return selection;
		} catch(Exception ex) {
			logger.error(String.format("Failed to replace items: examId=%s; studentId=%s; slot=%d", examId, studentId, slot), ex);
			Selection selection = new Selection();
			selection.setError(ex.getMessage());
			return selection;
		}
	}
	
	@RequestMapping(value="/replace/{examId}/{slot}", method = RequestMethod.POST)
	public Selection replaceItems(
			@PathVariable String examId, 
			@PathVariable int slot,
			@RequestBody StudentState studentState) 
	{
		try {
			Selection selection = studentService.replaceItems(examId, slot, studentState);
			return selection;
		} catch(Exception ex) {
			logger.error(String.format("Failed to replace items: examId=%s; slot=%d", examId, slot), ex);
			Selection selection = new Selection();
			selection.setError(ex.getMessage());
			return selection;
		}
	}

	@RequestMapping(value="/scores/{examId}/{studentId}/{slot}", method = RequestMethod.POST)
	public ResponseEntity<String> postScores(
			@PathVariable String examId, 
			@PathVariable String studentId, 
			@PathVariable int slot, 
			@RequestBody List<Integer> scores) 
	{
		if(scores != null && scores.size() > 0) {
			try {
				int[] arrScores = ArrayUtils.toPrimitive(scores.toArray(new Integer[0]));
				studentService.updateScores(examId, studentId, slot, arrScores);
				return new ResponseEntity<String>(HttpStatus.CREATED);
			} catch(Exception ex) {
				logger.error(String.format("Failed to update scores: examId=%s; studentId=%s; slot=%d", examId, studentId, slot), ex);
				return new ResponseEntity<String>(ex.getMessage(), HttpStatus.BAD_REQUEST);
			}
		} else {
			logger.error(String.format("Failed to update scores. Scores are required: examId=%s; studentId=%s; slot=%d", examId, studentId, slot));
			return new ResponseEntity<String>("Scores are required", HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value="/data/{examId}/{studentId}/{strandLevel}", method = RequestMethod.GET)
	public String getTestData(
			@PathVariable String examId, 
			@PathVariable String studentId, 
			@PathVariable boolean strandLevel) 
	{
		try {
			String testData = studentService.getStudentTestData(examId, studentId, strandLevel);
			return testData;
		} catch(Exception ex) {
			logger.error(String.format("Failed to get test data: examId=%s; studentId=%s; strandLevel=%s", examId, studentId, strandLevel), ex);
			return ex.toString();
		}
	}
	
	@RequestMapping(value="/data/{examId}/{strandLevel}", method = RequestMethod.POST)
	public String getTestDataFromState(
			@PathVariable String examId, 
			@PathVariable boolean strandLevel,
			@RequestBody StudentState studentState) 
	{
		try {
			String testData = studentService.getStudentTestData(examId, studentState, strandLevel);
			return testData;
		} catch(Exception ex) {
			logger.error(String.format("Failed to get test data: examId=%s; strandLevel=%s", examId, strandLevel), ex);
			return ex.toString();
		}
	}
	
	@RequestMapping(value="/estimate/{examId}/{studentId}", method = RequestMethod.GET)
	public Map<String,Map<String,Object>> getEstimate(
			@PathVariable String examId, 
			@PathVariable String studentId)
	{
		try {
			Map<String,Map<String,Object>> estimate = studentService.getFinalPerformanceEstimate(examId, studentId);
			return estimate;
		} catch(Exception ex) {
			logger.error(String.format("Failed to get final performance estimate data: examId=%s; studentId=%s", examId, studentId), ex);
			Map<String, Object> innerMap = new Hashtable<String, Object>();
			innerMap.put("error", ex.toString());
			Map<String, Map<String,Object>> outerMap = new Hashtable<String, Map<String,Object>>();
			outerMap.put("error", innerMap);
			return outerMap;
		}
	}
	
	@RequestMapping(value="/estimate/{examId}", method = RequestMethod.POST)
	public Map<String,Map<String,Object>> getEstimateFromState(
			@PathVariable String examId, 
			@RequestBody StudentState studentState)
	{
		try {
			Map<String,Map<String,Object>> estimate = studentService.getFinalPerformanceEstimate(examId, studentState);
			return estimate;
		} catch(Exception ex) {
			logger.error(String.format("Failed to get final performance estimate data: examId=%s", examId), ex);
			Map<String, Object> innerMap = new Hashtable<String, Object>();
			innerMap.put("error", ex.toString());
			Map<String, Map<String,Object>> outerMap = new Hashtable<String, Map<String,Object>>();
			outerMap.put("error", innerMap);
			return outerMap;
		}
	}
	
	@RequestMapping(value="/delete/{examId}/{studentId}", method = RequestMethod.DELETE)
	public ResponseEntity<String> delete(
			@PathVariable String examId,
			@PathVariable String studentId)
	{
		try {
			studentService.delete(examId, studentId);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch(Exception ex) {
			logger.error("Failed to delete student: examId=%s; studentId=%s", ex);
			return new ResponseEntity<String>(ex.toString(), HttpStatus.BAD_REQUEST);
		}
	}
}
