/**
 * 
 */
package org.parcc.cat.dcm.controller;

import java.util.Set;

import org.parcc.cat.dcm.bean.PoolMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author alex.filipovik
 *
 */
@RestController
public class DiagController {
	private static final Logger logger = LoggerFactory.getLogger(DiagController.class);
	
	@Autowired
	private PoolMap poolMap;

	@RequestMapping(value="/diag/exams", method = RequestMethod.GET)
	public Object[] getDiag() 
	{
		try {
			Set<String> keys = poolMap.keySet();
			return keys.toArray();
		} catch(Exception ex) {
			logger.error("Failed to get pool map keys", ex);
			return null;
		}
	}

}
