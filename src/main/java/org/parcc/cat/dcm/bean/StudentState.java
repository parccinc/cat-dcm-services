/**
 * 
 */
package org.parcc.cat.dcm.bean;

/**
 * @author alex.filipovik
 *
 */
public class StudentState {
	/**
	 * Base64 encoded student state.
	 */
	private String state;
	
	/**
	 * Is student in reviewable section.
	 */
	private boolean reviewable;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean isReviewable() {
		return reviewable;
	}

	public void setReviewable(boolean reviewable) {
		this.reviewable = reviewable;
	}
}
