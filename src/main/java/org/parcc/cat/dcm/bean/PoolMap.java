/**
 * 
 */
package org.parcc.cat.dcm.bean;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Hashtable;

import javax.annotation.PostConstruct;

import org.parcc.cat.dcm.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

/**
 * @author alex.filipovik
 * Singleton container of pools per input text file.
 */
public class PoolMap extends Hashtable<String, Pool> implements BeanFactoryAware {
	
	private static final long serialVersionUID = 5114134997306812720L;

	private static final Logger logger = LoggerFactory.getLogger(PoolMap.class);
	
	@Value("${dir.input}")
	private String dirInputPath;
	
	private BeanFactory beanFactory;
	
	@PostConstruct
	public void init() throws Exception {
		String inputDir = Application.getInputDir();
		if(!StringUtils.isEmpty(inputDir)) {
			this.dirInputPath = inputDir;
		}
		
		logger.info("Loading input files from " + dirInputPath);
		Path pathDirInput = Paths.get(dirInputPath);
		if(!Files.exists(pathDirInput)) {
			String msg = "Directory not found: " + pathDirInput;
			logger.error(msg);
			throw new IllegalArgumentException(msg);
		}
		
		if(!Files.isDirectory(pathDirInput)) {
			String msg = "Path " + pathDirInput + " is not a directory.";
			logger.error(msg);
			throw new IllegalArgumentException(msg);
		}
		
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(pathDirInput)) {
			for (Path entry : stream) {
				Pool pool = (Pool)beanFactory.getBean("pool");
				pool.init(entry);
				this.put(entry.getFileName().toString(), pool);
			}
		}
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}
}
