/**
 * 
 */
package org.parcc.cat.dcm.bean;

import java.util.List;

/**
 * @author alex.filipovik
 * Selection of item ids starting at the given slot.
 */
public class Selection {
	private String passageId;
	private List<Item> items;
	private String error;
	private StudentState studentState;
	
	public String getPassageId() {
		return passageId;
	}
	
	public void setPassageId(String passageId) {
		this.passageId = passageId;
	}
	
	public List<Item> getItems() {
		return items;
	}
	
	public void setItems(List<Item> itemList) {
		this.items = itemList;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public StudentState getStudentState() {
		return studentState;
	}

	public void setStudentState(StudentState studentState) {
		this.studentState = studentState;
	}
}
