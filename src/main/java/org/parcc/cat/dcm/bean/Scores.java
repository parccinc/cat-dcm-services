/**
 * 
 */
package org.parcc.cat.dcm.bean;

/**
 * @author alex.filipovik
 * Scores exchanged in requests and responses.
 */
public class Scores {
	/**
	 * The index of the first item that should be updated with the first given score
	 */
	private int slot;
	
	/**
	 * Array of slot scores.
	 */
	private int[] scores;
	
	private StudentState studentState;
	
	public int getSlot() {
		return slot;
	}
	
	public void setSlot(int slot) {
		this.slot = slot;
	}

	public int[] getScores() {
		return scores;
	}

	public void setScores(int[] scores) {
		this.scores = scores;
	}

	public StudentState getStudentState() {
		return studentState;
	}

	public void setStudentState(StudentState studentState) {
		this.studentState = studentState;
	}
}
