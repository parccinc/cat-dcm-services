/**
 * 
 */
package org.parcc.cat.dcm.bean;

/**
 * @author alex.filipovik
 * Selected item.
 */
public class Item {
	private String id;
	private boolean reviewable;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public boolean isReviewable() {
		return reviewable;
	}
	
	public void setReviewable(boolean reviewable) {
		this.reviewable = reviewable;
	}
}
