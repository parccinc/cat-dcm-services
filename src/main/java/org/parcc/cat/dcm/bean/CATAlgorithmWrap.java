/**
 * 
 */
package org.parcc.cat.dcm.bean;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import com.pearson.psy.cat.CATAlgorithm;
import com.pearson.psy.cat.CATException;

/**
 * @author alex.filipovik
 * Wraps CATAlgorithm to provide default constructor in order to build
 * the pool dynamically at start-up.
 */
public class CATAlgorithmWrap {

	private Path inputFilePath;
	private CATAlgorithm catAlgorithm;
	
	public CATAlgorithm getCatAlgorithm() {
		return catAlgorithm;
	}

	public void setCatAlgorithm(CATAlgorithm catAlgorithm) {
		this.catAlgorithm = catAlgorithm;
	}

	public Path getInputFilePath() {
		return inputFilePath;
	}

	public void setInputFile(Path inputFilePath) throws IOException, CATException {
		this.inputFilePath = inputFilePath;
		byte[] bytes = Files.readAllBytes(inputFilePath);
		String inputText = new String(bytes);
		this.catAlgorithm = new CATAlgorithm(inputText, null);
	}
}
