/**
 * 
 */
package org.parcc.cat.dcm.bean;

import org.parcc.cat.dcm.Application;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author alex.filipovik
 * Customize embedded Tomcat.
 */
@Component
public class Customization implements EmbeddedServletContainerCustomizer {

	/* (non-Javadoc)
	 * @see org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer#customize(org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer)
	 */
	@Override
	public void customize(ConfigurableEmbeddedServletContainer container) {
		String port = Application.getPort();
		if(!StringUtils.isEmpty(port)) {
			container.setPort(Integer.parseInt(port));
		}
		
		String contextPath = Application.getContext();
		if(!StringUtils.isEmpty(contextPath)) {
			container.setContextPath(contextPath);
		}
	}

}
