/**
 * 
 */
package org.parcc.cat.dcm.bean;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.springframework.aop.target.CommonsPoolTargetSource;

/**
 * @author alex.filipovik
 * Extends the spring's CommonsPoolTargetSource and adds an init method
 * because default implementation does nothing with minIdle property.
 */
public class Pool extends CommonsPoolTargetSource {
	
	private Path inputFilePath;
	
	@Override
	public Object getTarget() throws Exception {
		CATAlgorithmWrap target = (CATAlgorithmWrap)super.getTarget();
		target.setInputFile(inputFilePath);
		return target;
	}


	public void init(Path inputFilePath) throws Exception {
		this.inputFilePath = inputFilePath;
		List<Object> beans = new ArrayList<Object>();
		int minIdle = this.getMinIdle();
		for(int i = 0; i < minIdle; i++) {
			CATAlgorithmWrap target = (CATAlgorithmWrap)this.getTarget();
			beans.add(target);
		}
		
		for(Object target : beans) {
			this.releaseTarget(target);
		}
	}

	public Path getInputFilePath() {
		return inputFilePath;
	}

	public void setInputFile(Path inputFilePath) {
		this.inputFilePath = inputFilePath;
	}
}
