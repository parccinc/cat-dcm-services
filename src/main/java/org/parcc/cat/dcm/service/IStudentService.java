/**
 * 
 */
package org.parcc.cat.dcm.service;

import java.io.IOException;
import java.util.Map;

import org.parcc.cat.dcm.bean.Scores;
import org.parcc.cat.dcm.bean.Selection;
import org.parcc.cat.dcm.bean.StudentState;

/**
 * @author alex.filipovik
 * Student service.
 */
public interface IStudentService {
	/**
	 * Returns a new selection of item ids starting at the given slot. Zero to many items may be returned.
	 * @param examId
	 * @param studentId
	 * @param slot - the index where a new item selection should be made.
	 * @return A new selection of item ids starting at the given slot.
	 * @throws Exception
	 */
	public Selection getItems(String examId, String studentId, int slot) throws Exception;
	
	/**
	 * Returns a new selection of item ids which will replace the items currently at this slot. The item 
	 * currently at this slot and any items after it are discarded. Depending on the test configuration, 
	 * the same items may be returned, or a different number of items, or no items indicating that the 
	 * items will not be replaced and the test has terminated. 
	 * The given slot number should be the first un-scored item. Once an item has been scored, it cannot be replaced. 
	 * @param examId
	 * @param studentId
	 * @param slot - the index of the item that should be discarded and replaced.
	 * @return A new selection of item ids which will replace the items currently at this slot.
	 */
	public Selection replaceItems(String examId, String studentId, int slot) throws Exception;
	
	/**
	 * Returns a new selection of item ids which will replace the items currently at this slot. The item 
	 * currently at this slot and any items after it are discarded. Depending on the test configuration, 
	 * the same items may be returned, or a different number of items, or no items indicating that the 
	 * items will not be replaced and the test has terminated. 
	 * The given slot number should be the first un-scored item. Once an item has been scored, it cannot be replaced. 
	 * @param examId
	 * @param slot - the index of the item that should be discarded and replaced.
	 * @param state - StudentState instance.
	 * @return A new selection of item ids which will replace the items currently at this slot.
	 */
	public Selection replaceItems(String examId, int slot, StudentState state) throws Exception;
	
	/**
	 * Assigns the given scores to the items starting at the specified slot. The first 
	 * score will be assigned to the given slot, and each additional score to the next 
	 * slot after that. If the item at the specified slot already had a score, that score 
	 * is overwritten and any PerformanceEstimates for this slot or afterward are invalid 
	 * and will be discarded.
	 * @param examId
	 * @param studentId
	 * @param slot - the index of the item that should be updated with the given score
	 * @param scores - the score that should be assigned to the item at the given slot
	 * @throws Exception 
	 */
	public void updateScores(String examId, String studentId, int slot, int[] scores) throws Exception;
	
	/**
	 * Updates scores and selects next items.
	 * @param examId
	 * @param scores - For the first call, this is null.
	 * @return A new selection of item ids.
	 * @throws Exception
	 */
	public Selection updateScoreAndSelectItems(String examId, Scores scores) throws Exception;
	
	/**
	 * Returns a human-readable representation of the current student state. The data will 
	 * be formatted as a table, with one line per item assigned to the student, where each 
	 * line is composed of whitespace-separated columns.
	 * The table returned will include lines ONLY for assigned items from the original inputText.
	 * @param examId
	 * @param studentId
	 * @param strandLevel - if true, each line's performance estimate columns will contain 
	 * strand-level values for the strand assigned to that line's slot. If false, each line 
	 * will contain the "overall" estimate. 
	 * @return A human-readable representation of the current student state. 
	 * @throws Exception 
	 */
	public String getStudentTestData(String examId, String studentId, boolean strandLevel) throws Exception;
	
	/**
	 * Returns a human-readable representation of the current student state. The data will 
	 * be formatted as a table, with one line per item assigned to the student, where each 
	 * line is composed of whitespace-separated columns.
	 * The table returned will include lines ONLY for assigned items from the original inputText.
	 * @param examId
	 * @param studentState
	 * @param strandLevel - if true, each line's performance estimate columns will contain 
	 * strand-level values for the strand assigned to that line's slot. If false, each line 
	 * will contain the "overall" estimate. 
	 * @return A human-readable representation of the current student state. 
	 * @throws Exception 
	 */
	public String getStudentTestData(String examId, StudentState studentState, boolean strandLevel) throws Exception;
	
	/**
	 * Returns an object map of final performance estimates for all strands in the test, suitable for 
	 * serialization into JSON. Strands with no operational items assigned may be ommitted from the 
	 * returned map. The method returns null if no strands have performance estimates. 
	 * The top level of the object map is a Map of strand names to strand performance estimates. 
	 * The second level of the object map is a strand performance estimate Map of name/value pairs. 
	 * Every strand-specific map is guaranteed to include a pair with a name of "model", and a value 
	 * that indicates which PsychometricModel was used to calculate the performance estimate. The other 
	 * pairs will be model-specific. 
	 * @param examId
	 * @param studentId
	 * @return An object map of final performance estimates for all strands in the test. 
	 * @throws Exception 
	 */
	public Map<String,Map<String,Object>> getFinalPerformanceEstimate(String examId, String studentId) throws Exception;
	
	/**
	 * Returns an object map of final performance estimates for all strands in the test, suitable for 
	 * serialization into JSON. Strands with no operational items assigned may be ommitted from the 
	 * returned map. The method returns null if no strands have performance estimates. 
	 * The top level of the object map is a Map of strand names to strand performance estimates. 
	 * The second level of the object map is a strand performance estimate Map of name/value pairs. 
	 * Every strand-specific map is guaranteed to include a pair with a name of "model", and a value 
	 * that indicates which PsychometricModel was used to calculate the performance estimate. The other 
	 * pairs will be model-specific. 
	 * @param examId
	 * @param studentState
	 * @return An object map of final performance estimates for all strands in the test. 
	 * @throws Exception 
	 */
	public Map<String,Map<String,Object>> getFinalPerformanceEstimate(String examId, StudentState studentState) throws Exception;
	
	/**
	 * Deletes student data.
	 * @param examId
	 * @param studentId
	 * @throws IOException 
	 */
	public void delete(String examId, String studentId) throws IOException;
}
