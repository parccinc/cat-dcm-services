/**
 * 
 */
package org.parcc.cat.dcm.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.parcc.cat.dcm.Application;
import org.parcc.cat.dcm.bean.CATAlgorithmWrap;
import org.parcc.cat.dcm.bean.Item;
import org.parcc.cat.dcm.bean.Pool;
import org.parcc.cat.dcm.bean.PoolMap;
import org.parcc.cat.dcm.bean.Scores;
import org.parcc.cat.dcm.bean.Selection;
import org.parcc.cat.dcm.bean.StudentState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.pearson.psy.cat.CATAlgorithm;

/**
 * @author alex.filipovik
 * Student service implementation.
 */
@Service
public class StudentService implements IStudentService {
	
	@Value("${dir.student}")
	private String dirStudentPath;
	
	@Autowired
	private PoolMap poolMap;
	
	@Value("${test.data.precision}")
	private int precision;
	
	private CATAlgorithmWrap algorithmWrap;
	
	public StudentService() {
		String precision = Application.getTestDataPrecision();
		if(!StringUtils.isEmpty(precision)) {
			this.precision = Integer.parseInt(precision);
		}
		
		String studentDir = Application.getStudentDir();
		if(!StringUtils.isEmpty(studentDir)) {
			this.dirStudentPath = studentDir;
		}
	}

	@Override
	public Selection getItems(String examId, String studentId, int slot) throws Exception {
		if(StringUtils.isEmpty(examId)) {
			throw new IllegalArgumentException("Exam ID is required");
		}
		
		if(StringUtils.isEmpty(studentId)) {
			throw new IllegalArgumentException("Student ID is required");
		}
		
		if(slot < 0) {
			throw new IllegalArgumentException("Slot must be >= 0");
		}
		
		Path path = Paths.get(dirStudentPath);
		if(!Files.exists(path) || !Files.isDirectory(path)) {
			Files.createDirectories(path);
		}
		
		try {
			CATAlgorithm algorithm = getAlgorithm(examId, studentId);
			String selectionString = algorithm.selectNextItems(slot);
			Selection selection = parseSelectionString(selectionString, examId, studentId, false);
			serializeStudent(algorithm, examId, studentId);
			return selection;
		} finally {
			releaseAlgorithm(examId);
		}
	}

	@Override
	public Selection replaceItems(String examId, String studentId, int slot) throws Exception {
		if(StringUtils.isEmpty(examId)) {
			throw new IllegalArgumentException("Exam ID is required");
		}
		
		if(StringUtils.isEmpty(studentId)) {
			throw new IllegalArgumentException("Student ID is required");
		}
		
		if(slot < 0) {
			throw new IllegalArgumentException("Slot must be >= 0");
		}
		
		Path path = Paths.get(dirStudentPath);
		if(!Files.exists(path) || !Files.isDirectory(path)) {
			throw new IllegalArgumentException("Student directory not found: " + path);
		}
		
		try {
			CATAlgorithm algorithm = getAlgorithm(examId, studentId);
			String selectionString = algorithm.replaceItems(slot);
			Selection selection = parseSelectionString(selectionString, examId, studentId, false);
			serializeStudent(algorithm, examId, studentId);
			return selection;
		} finally {
			releaseAlgorithm(examId);
		}
	}

	@Override
	public Selection replaceItems(String examId, int slot, StudentState state) throws Exception {
		if(StringUtils.isEmpty(examId)) {
			throw new IllegalArgumentException("Exam ID is required");
		}
		
		if(slot < 0) {
			throw new IllegalArgumentException("Slot must be >= 0");
		}
		
		if(state == null) {
			throw new IllegalArgumentException("Student state is required");
		}
		
		try {
			CATAlgorithm algorithm = getAlgorithmFromState(examId, state.getState());
			String selectionString = algorithm.replaceItems(slot);
			boolean reviewable = state.isReviewable();
			Selection selection = parseSelectionString(selectionString, examId, null, reviewable);
			serializeStudent(algorithm, selection);
			return selection;
		} finally {
			releaseAlgorithm(examId);
		}
	}

	@Override
	public void updateScores(String examId, String studentId, int slot, int[] scores) throws Exception {
		if(StringUtils.isEmpty(examId)) {
			throw new IllegalArgumentException("Exam ID is required");
		}
		
		if(StringUtils.isEmpty(studentId)) {
			throw new IllegalArgumentException("Student ID is required");
		}
		
		if(slot < 0) {
			throw new IllegalArgumentException("Slot must be >= 0");
		}
		
		Path path = Paths.get(dirStudentPath);
		if(!Files.exists(path) || !Files.isDirectory(path)) {
			throw new IllegalArgumentException("Student directory not found: " + path);
		}
		
		try {
			CATAlgorithm algorithm = getAlgorithm(examId, studentId);
			algorithm.updateScores(slot, scores);
			serializeStudent(algorithm, examId, studentId);
		} finally {
			releaseAlgorithm(examId);
		}
	}
	
	@Override
	public Selection updateScoreAndSelectItems(String examId, Scores scores) throws Exception {
		if(StringUtils.isEmpty(examId)) {
			throw new IllegalArgumentException("Exam ID is required");
		}
		
		if(scores != null && scores.getStudentState() == null && StringUtils.isEmpty(scores.getStudentState().getState())) {
			throw new IllegalArgumentException("Student state is required in Scores.");
		}
		
		CATAlgorithm algorithm = null;
		try {
			int nextSlot = 0;
			int slot = 0;
			boolean reviewable = false;
			if(scores == null) {
				algorithm = getAlgorithmFromState(examId, null);
			} else {
				slot = scores.getSlot();
				int[] arrScores = scores.getScores();
				String state = scores.getStudentState().getState();
				algorithm = getAlgorithmFromState(examId, state);
				algorithm.updateScores(slot, arrScores);
				nextSlot = slot + arrScores.length;
				reviewable = scores.getStudentState().isReviewable();
			}
			
			String selectionString = algorithm.selectNextItems(nextSlot);
			Selection selection = parseSelectionString(selectionString, examId, null, reviewable);
			serializeStudent(algorithm, selection);
			return selection;
		} finally {
			if(algorithm != null) {
				releaseAlgorithm(examId);
			}
		}
	}

	@Override
	public String getStudentTestData(String examId, String studentId, boolean strandLevel) throws Exception {
		if(StringUtils.isEmpty(examId)) {
			throw new IllegalArgumentException("Exam ID is required");
		}
		
		if(StringUtils.isEmpty(studentId)) {
			throw new IllegalArgumentException("Student ID is required");
		}
		
		try {
			CATAlgorithm algorithm = getAlgorithm(examId, studentId);
			return algorithm.getStudentTestData(precision, strandLevel);
		} finally {
			releaseAlgorithm(examId);
		}
	}

	@Override
	public String getStudentTestData(String examId, StudentState studentState, boolean strandLevel) throws Exception {
		if(StringUtils.isEmpty(examId)) {
			throw new IllegalArgumentException("Exam ID is required");
		}
		
		if(studentState == null) {
			throw new IllegalArgumentException("Student state is required");
		} else {
			if(StringUtils.isEmpty(studentState.getState())) {
				throw new IllegalArgumentException("Student state is required");
			}
		}
		
		try {
			String state = studentState.getState();
			CATAlgorithm algorithm = getAlgorithmFromState(examId, state);
			return algorithm.getStudentTestData(precision, strandLevel);
		} finally {
			releaseAlgorithm(examId);
		}
	}

	@Override
	public Map<String,Map<String,Object>> getFinalPerformanceEstimate(String examId, String studentId) throws Exception {
		if(StringUtils.isEmpty(examId)) {
			throw new IllegalArgumentException("Exam ID is required");
		}
		
		if(StringUtils.isEmpty(studentId)) {
			throw new IllegalArgumentException("Student ID is required");
		}
		
		try {
			CATAlgorithm algorithm = getAlgorithm(examId, studentId);
			return algorithm.getFinalPerformanceEstimateJSON();
		} finally {
			releaseAlgorithm(examId);
		}
	}

	@Override
	public Map<String,Map<String,Object>> getFinalPerformanceEstimate(String examId, StudentState studentState) throws Exception {
		if(StringUtils.isEmpty(examId)) {
			throw new IllegalArgumentException("Exam ID is required");
		}
		
		if(studentState == null) {
			throw new IllegalArgumentException("Student state is required");
		} else {
			if(StringUtils.isEmpty(studentState.getState())) {
				throw new IllegalArgumentException("Student state is required");
			}
		}
		
		try {
			String state = studentState.getState();
			CATAlgorithm algorithm = getAlgorithmFromState(examId, state);
			return algorithm.getFinalPerformanceEstimateJSON();
		} finally {
			releaseAlgorithm(examId);
		}
	}
	
	@Override
	public void delete(String examId, String studentId) throws IOException {
		Files.deleteIfExists(getStudentFilePath(examId, studentId));
		Files.deleteIfExists(getStudentReviewableFilePath(examId, studentId));
	}
	
	/**
	 * Parses selection string returned from CATAlgorithm selectNextItems() or replaceItems().
	 * @param selectionString - output from CATAlgorithm selectNextItems() or replaceItems().
	 * @param examId
	 * @param studentId - can be null when student state is not on file system.
	 * @param currentReviewable - ignored when studentId is not null.
	 * @return Returns a Selection instance. When studentId is null, its StudentState instance is set
	 * with null state, and valid reviewable.
	 * @throws Exception
	 */
	private Selection parseSelectionString(String selectionString, String examId, String studentId, boolean currentReviewable) throws Exception {
		Selection selection = new Selection();
		String[] parts = selectionString.split(" ");
		String passageId = parts[0];
		if(!passageId.equals(".")) {
			selection.setPassageId(passageId);
		}
		
		int len = parts.length;
		boolean reviewable = currentReviewable;
		boolean reviewableFileExists = false;
		Path reviewablePath = null;
		if(studentId != null) {
			reviewablePath = getStudentReviewableFilePath(examId, studentId);
			reviewableFileExists = Files.exists(reviewablePath);
			reviewable = reviewableFileExists;
		}
		
		List<Item> items = new ArrayList<Item>();
		for(int i = 2; i < len; i++) {
			String part = parts[i];
			if(part.equals("<r")) {
				reviewable = true;
			} else if(part.equals("r>")) {
				reviewable = false;
			} else {
				Item item = new Item();
				item.setId(part);
				item.setReviewable(reviewable);
				items.add(item);
			}
		}
		
		selection.setItems(items);
		if(studentId == null) {
			StudentState studentState = new StudentState();
			studentState.setReviewable(reviewable);
			selection.setStudentState(studentState);
		} else {
			if(reviewable && !reviewableFileExists) {
				Files.createFile(reviewablePath);
			} else if(!reviewable && reviewableFileExists) {
				Files.delete(reviewablePath);
			}
		}
		
		return selection;
	}

	/**
	 * Creates student or deserializes existing student.
	 * @param examId
	 * @param studentId
	 * @return A CATAlgorithm instance.
	 * @throws Exception
	 */
	private CATAlgorithm getAlgorithm(String examId, String studentId) throws Exception {
		CATAlgorithm algorithm = getAlgorithm(examId);
		Path studentPath = getStudentFilePath(examId, studentId);
		if(!StringUtils.isEmpty(studentId) && Files.exists(studentPath) && Files.isRegularFile(studentPath)) {
			byte[] studentState = Files.readAllBytes(studentPath);
			algorithm.deserializeStudent(studentState);
		} else {
			int seed = new Random().nextInt(CATAlgorithm.MAX_SEED - CATAlgorithm.MIN_SEED) + CATAlgorithm.MIN_SEED;
			algorithm.createStudent(studentId, seed);
		}
		
		return algorithm;
	}

	/**
	 * Creates student or deserializes existing student.
	 * @param examId
	 * @param studentId
	 * @return A CATAlgorithm instance.
	 * @throws Exception
	 */
	private CATAlgorithm getAlgorithmFromState(String examId, String state) throws Exception {
		CATAlgorithm algorithm = getAlgorithm(examId);
		if(StringUtils.isEmpty(state)) {
			String studentId = RandomStringUtils.randomAlphanumeric(10);
			int seed = new Random().nextInt(CATAlgorithm.MAX_SEED - CATAlgorithm.MIN_SEED) + CATAlgorithm.MIN_SEED;
			algorithm.createStudent(studentId, seed);
		} else {
			byte[] studentState = Base64.getDecoder().decode(state);
			algorithm.deserializeStudent(studentState);
		}
		
		return algorithm;
	}
	
	/**
	 * Gets algorithm from pool.
	 * @param examId
	 * @return A CATAlgorithm instance initialized with input file.
	 * @throws Exception
	 */
	private CATAlgorithm getAlgorithm(String examId) throws Exception {
		Pool pool = poolMap.get(examId + ".txt");
		if(pool == null) {
			throw new IllegalArgumentException(String.format("Exam not found; examId=%s", examId));
		}
		
		this.algorithmWrap = (CATAlgorithmWrap)pool.getTarget();
		CATAlgorithm algorithm = algorithmWrap.getCatAlgorithm();
		return algorithm;
	}
	
	/**
	 * Serializes student state.
	 * @param algorithm - the CATAlgorithm instance.
	 * @param examId
	 * @param studentId
	 * @throws Exception
	 */
	private void serializeStudent(CATAlgorithm algorithm, String examId, String studentId) throws Exception {
		byte[] studentState = algorithm.serializeStudent();
		Path studentPath = getStudentFilePath(examId, studentId);
		Files.write(studentPath, studentState);
	}
	
	/**
	 * Serializes student state.
	 * @param algorithm - the CATAlgorithm instance.
	 * @param examId
	 * @param studentId
	 * @throws Exception
	 */
	private void serializeStudent(CATAlgorithm algorithm, Selection selection) throws Exception {
		byte[] studentState = algorithm.serializeStudent();
		String encodedState = Base64.getEncoder().encodeToString(studentState);
		selection.getStudentState().setState(encodedState);
	}
	
	/**
	 * Gets serialized student path.
	 * @param examId
	 * @param studentId
	 * @return A Path instance.
	 */
	private Path getStudentFilePath(String examId, String studentId) {
		return Paths.get(dirStudentPath, getStudentFileNameBase(examId, studentId) + ".bin");
	}
	
	/**
	 * Gets student reviewable file path.
	 * @param examId
	 * @param studentId
	 * @return A Path instance.
	 */
	private Path getStudentReviewableFilePath(String examId, String studentId) {
		return Paths.get(dirStudentPath, getStudentFileNameBase(examId, studentId) + ".rev");
	}
	
	/**
	 * Creates student file name base.
	 * @param examId
	 * @param studentId
	 * @return A string with student file name base.
	 */
	private String getStudentFileNameBase(String examId, String studentId) {
		return studentId + "-" + examId;
	}
	
	/**
	 * Releases algorithm obtained from a pool.
	 * @param examId
	 * @throws Exception
	 */
	private void releaseAlgorithm(String examId) throws Exception {
		if(this.algorithmWrap != null) {
			Pool pool = poolMap.get(examId + ".txt");
			if(pool == null) {
				throw new IllegalArgumentException("Algorithm release failed. Pool not found; examId=" + examId);
			}
			
			pool.releaseTarget(this.algorithmWrap);
		}
	}
}
