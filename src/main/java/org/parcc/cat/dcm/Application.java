/**
 * 
 */
package org.parcc.cat.dcm;

import org.parcc.cat.dcm.bean.CATAlgorithmWrap;
import org.parcc.cat.dcm.bean.Pool;
import org.parcc.cat.dcm.bean.PoolMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

/**
 * @author alex.filipovik
 *
 */
@ComponentScan
@Configuration
@EnableAutoConfiguration
public class Application {
	@Value("${pool.max.idle}")
	private int poolMaxIdle;
	
	@Value("${pool.min.idle}")
	private int poolMinIdle;
	
	@Value("${pool.max.size}")
	private int poolMaxSize;
	
	private static String port;
	private static String context;
	private static String inputDir;
	private static String studentDir;
	private static String testDataPrecision;
	private static String strPoolMaxSize;
	private static String strPoolMaxIdle;
	private static String strPoolMinIdle;

	public static void main(String[] args) {
		for (String s: args) {
            String[] arr = s.split("=");
            if(arr.length == 2) {
            	switch(arr[0]) {
            	case "port":
            		if(isInteger(arr[1])) {
            			port = arr[1];
            		}
            		break;
            		
            	case "context":
            		context = arr[1];
            		break;
            		
            	case "inputdir":
            		inputDir = arr[1];
            		break;
            		
            	case "studentdir":
            		studentDir = arr[1];
            		break;
            		
            	case "testdataprecision":
            		if(isInteger(arr[1])) {
            			testDataPrecision = arr[1];
            		}
            		break;
            		
            	case "poolmaxsize":
            		if(isInteger(arr[1])) {
            			strPoolMaxSize = arr[1];
            		}
            		break;
            		
            	case "poolminidle":
            		if(isInteger(arr[1])) {
            			strPoolMinIdle = arr[1];
            		}
            		break;
            		
            	case "poolmaxidle":
            		if(isInteger(arr[1])) {
            			strPoolMaxIdle = arr[1];
            		}
            		break;
            	}
            }
        }

        SpringApplication.run(Application.class, args);
    }
	
	public static String getPort() {
		return port;
	}
	
	public static String getContext() {
		return context;
	}

	public static String getInputDir() {
		return inputDir;
	}

	public static String getStudentDir() {
		return studentDir;
	}

	public static String getTestDataPrecision() {
		return testDataPrecision;
	}
	
	private static boolean isInteger(String str) {
        return str.matches("^[0-9]+$");
    }
	
	@Bean(name = "poolMap")
	@Scope("singleton")
	public PoolMap createPoolMap() {
		return new PoolMap();
	}
	
	@Bean(name = "pool")
	@Scope("prototype")
	public Pool createPool() {
		Pool pool = new Pool();
		pool.setTargetBeanName("algorithm");
		pool.setTargetClass(CATAlgorithmWrap.class);
		if(!StringUtils.isEmpty(strPoolMaxIdle)) {
			poolMaxIdle = Integer.parseInt(strPoolMaxIdle);
		}
		
		if(!StringUtils.isEmpty(strPoolMinIdle)) {
			poolMinIdle = Integer.parseInt(strPoolMinIdle);
		}
		
		if(!StringUtils.isEmpty(strPoolMaxSize)) {
			poolMaxSize = Integer.parseInt(strPoolMaxSize);
		}
		
		pool.setMaxIdle(poolMaxIdle);
		pool.setMinIdle(poolMinIdle);
		pool.setMaxSize(poolMaxSize);
		return pool;
	}
	
	@Bean(name = "algorithm")
	@Scope("prototype")
	public CATAlgorithmWrap createAlgorithm() {
		return new CATAlgorithmWrap();
	}
}
