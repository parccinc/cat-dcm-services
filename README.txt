This is a POC implementation of PARCC CAT DCM Service. It wraps CAT 3.0 
library cat-core-3.0.18.jar.

The service is Java executable, using Spring Boot with embedded Tomcat.

Command line: java -jar this.jar port=8080 context=/context inputdir=/inputDir studentdir=/studentDir testdataprecision=5 poolmaxsize=100 poolmaxidle=20 poolminidle=10
Where all arguments are optional and will default to application.properties:
port Port number of the service
context Sets the context path for the embedded servlet container. The context should start with a "/" character but not end with a "/" character. The default context path can be specified using an empty string.
inputdir Directory for input files
studentdir Directory for student files - not used when student state is sent back and forth between client and server 
testdataprecision The number of fraction digits after the decimal point in any floating-point columns in test results
poolmaxsize Max number of CATAlgorithm instances in a pool
poolmaxidle Max number of idle CATAlgorithm instances in a pool
poolminidle Min number of idle CATAlgorithm instances in a pool

When started, a collection of pools is created for every input text file 
found at dir.input location. A pool entry is a CATAlgorithm instance 
initialized with input text file, and reused when request for its exam 
is serviced.

When request is serviced, attempt is made to deserialize student data 
from dir.student location. If student data is not found, new student 
is created.

Student data is serialized after each completion of serviced request 
(when student data has changed).

Services:
--------------------------------------------------------------------
POST /items/{examId} - updates score(s) and gets item(s)

String examId

This request is used to update scores and select items with one request.

Request payload is JSON formatted Scores object:
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "Scores",
  "type": "object",
  "properties": {
    "slot": {
      "description": "The index of the first item that should be updated with the first given score",
      "type": "integer"
    },
    "scores": {
      "description": "Array of slot scores",
      "type": "array",
      "items": {
          "type": "integer"
      }
    },
    "studentState": {
      "description": "The student state. It isn't required only in the first call.",
      "type": "object",
      "properties": {
        "state": {
          "description": "Base64 encoded student state",
          "type": "string"
        },
        "reviewable": {
          "description": "Is student in reviewable range",
          "type": "boolean"
        }
      },
      "required": [
      	"studentState",
      	"reviewable"
      ]
    }
  },
  "required": [
    "slot",
    "scores"
  ]
}

When payload object is null, then items are selected for slot 0. Otherwise,
scores are updated and items are selected for slot = slot + array length.

Returns JSON formatted selection of items starting at the given slot:
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "Selection",
  "type": "object",
  "properties": {
    "passage": {
      "description": "Passage ID",
      "type": "string"
    },
    "items": {
      "description": "Array of selected items",
      "type": "array",
      "items": {
        "descripton": "Selected item",
        "type": "object",
        "properties": {
          "id": {
            "description": "Item ID",
            "type": "string"
          },
          "reviewable": {
            "description": "Is item in reviewable range",
            "type": "boolean"
          }
        }
      }
    },
    "error": {
      "description": "Error message",
      "type": "string"
    },
    "studentState": {
      "description": "The student state",
      "type": "object",
      "properties": {
        "state": {
          "description": "Base64 encoded student state",
          "type": "string"
        },
        "reviewable": {
          "description": "Is student in reviewable range",
          "type": "boolean"
        }
      },
      "required": [
      	"state",
      	"reviewable"
      ]
    }
  },
  "required": [
    "passage",
    "items",
    "error",
    "studentState"
  ]
}


Zero to many items may be returned. Zero items means test has terminated and the student is finished. 

See GET /items/{examId}/{studentId}/{slot} for information on passages.

--------------------------------------------------------------------
GET /items/{examId}/{studentId}/{slot} - gets item(s)

String examId
String studentId
int slot - the index where a new item selection should be made.

Returns JSON formatted selection of items starting at the given slot:
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "Selection",
  "type": "object",
  "properties": {
    "passage": {
      "description": "Passage ID",
      "type": "string"
    },
    "items": {
      "description": "Array of selected items",
      "type": "array",
      "items": {
        "descripton": "Selected item",
        "type": "object",
        "properties": {
          "id": {
            "description": "Item ID",
            "type": "string"
          },
          "reviewable": {
            "description": "Is item in reviewable range",
            "type": "boolean"
          }
        }
      }
    },
    "error": {
      "description": "Error message",
      "type": "string"
    },
    "studentState": {
      "description": "The student state - always null",
      "type": "object",
      "properties": {
        "state": {
          "description": "Base64 encoded student state",
          "type": "string"
        },
        "reviewable": {
          "description": "Is student in reviewable range",
          "type": "boolean"
        }
      },
      "required": [
      	"state",
      	"reviewable"
      ]
    }
  },
  "required": [
    "passage",
    "items",
    "error",
    "studentState"
  ]
}
 

Zero to many items may be returned. Zero items means test has terminated and the student is finished. 

Version 3.0.14 - When passage has more then one item, all items are selected in a single call.
Be careful of exiting/resuming on passages, though.  If you call replaceItems in the middle of a 
passage, a few different things can happen.  Lets say you're on the 4th item of a 6 item passage.

P1-1  P1-2  P1-3  P1-4  P1-5  P1-6
                   ^
                   |-current

Before you call replaceItems, you should discard the first unscored item, and everything after it.

P1-1  P1-2  P1-3  
                   ^
                   |-current

If the replace type is SAME, replaceItems will return the same items you just discarded.

P1-1  P1-2  P1-3  P1-4  P1-5  P1-6
                   ^
                   |-current

If the replace type is BLOCK, replaceItems can return anything... a single item, a new passage of 
a different size, or even zero items terminating the test.

P1-1  P1-2  P1-3  P2-1  P2-2  P2-3  P2-4
                   ^
                   |-current

If the replace type is PARTIAL, replaceItems will select a new passage of exactly the same size 
as the original, but it won't return all of it.  You will only get the tail of the new passage, 
equal to the number of items that you discarded.

P1-1  P1-2  P1-3  P3-4  P3-5  P3-6
                   ^
                   |-current

Your code should not need to know what the resume type is, I just describe them so you're aware 
of the possibilities.  The delivery system's behavior should be the same for all types.  If items 
were exposed to the student but not scored, replaceItems can be called on the first unscored item.  
The delivery system should discard that item and everything after it, then replace them with the 
new items returned by replaceItems.  Do not call this method on scored items.  Once an item is 
scored, it cannot be replaced.

You might be able to ignore the "terminate on resume" case.  It's a rare, ugly case and it can 
only happen on tests with three features:  multi-item passages, variable length, and the BLOCK 
resume type.  If none of your tests combine these three features, then this will never happen and 
you don't have to handle it.  The behavior occurs when you're in a passage beyond the minimum test 
length.  When the passage was selected, the termination rule was not yet satisfied.  But if you 
resume in the middle of that passage, it's possible the termination rule was satisfied partway 
through and there's no need to replace the discarded items.  Zero items will be returned, indicating 
termination.

--------------------------------------------------------------------
GET /replace/{examId}/{studentId}/{slot} - replaces item(s)

String examId
String studentId
int slot - the index of the item that should be discarded and replaced.

Returns JSON formatted selection of items which will replace the items currently at this slot.
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "Selection",
  "type": "object",
  "properties": {
    "passage": {
      "description": "Passage ID",
      "type": "string"
    },
    "items": {
      "description": "Array of selected items",
      "type": "array",
      "items": {
        "descripton": "Selected item",
        "type": "object",
        "properties": {
          "id": {
            "description": "Item ID",
            "type": "string"
          },
          "reviewable": {
            "description": "Is item in reviewable range",
            "type": "boolean"
          }
        }
      }
    },
    "error": {
      "description": "Error message",
      "type": "string"
    },
    "studentState": {
      "description": "The student state - always null",
      "type": "object",
      "properties": {
        "state": {
          "description": "Base64 encoded student state",
          "type": "string"
        },
        "reviewable": {
          "description": "Is student in reviewable range",
          "type": "boolean"
        }
      },
      "required": [
      	"state",
      	"reviewable"
      ]
    }
  },
  "required": [
    "passage",
    "items",
    "error",
    "studentState"
  ]
}

--------------------------------------------------------------------
POST /replace/{examId}/{slot} - replaces item(s)

String examId
int slot - the index of the item that should be discarded and replaced.

Request payload is JSON formatted StudentState object:
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "The StudentState",
  "type": "object",
  "properties": {
    "state": {
      "description": "Base64 encoded student state",
      "type": "string"
    },
    "reviewable": {
      "description": "Is student in reviewable range",
      "type": "boolean"
    }
  },
  "required": [
  	"studentState",
  	"reviewable"
  ]
}

Returns JSON formatted selection of items which will replace the items currently at this slot.
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "Selection",
  "type": "object",
  "properties": {
    "passage": {
      "description": "Passage ID",
      "type": "string"
    },
    "items": {
      "description": "Array of selected items",
      "type": "array",
      "items": {
        "descripton": "Selected item",
        "type": "object",
        "properties": {
          "id": {
            "description": "Item ID",
            "type": "string"
          },
          "reviewable": {
            "description": "Is item in reviewable range",
            "type": "boolean"
          }
        }
      }
    },
    "error": {
      "description": "Error message",
      "type": "string"
    },
    "studentState": {
      "description": "The student state",
      "type": "object",
      "properties": {
        "state": {
          "description": "Base64 encoded student state",
          "type": "string"
        },
        "reviewable": {
          "description": "Is student in reviewable range",
          "type": "boolean"
        }
      },
      "required": [
      	"state",
      	"reviewable"
      ]
    }
  },
  "required": [
    "passage",
    "items",
    "error",
    "studentState"
  ]
}

--------------------------------------------------------------------
POST /scores/{examId}/{studentId}/{slot} - updates scores

String examId
String studentId
int slot - the index of the item that should be discarded and replaced.

Request payload is JSON formatted Scores object:
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "Scores",
  "type": "object",
  "properties": {
    "slot": {
      "description": "The index of the first item that should be updated with the first given score",
      "type": "integer"
    },
    "scores": {
      "description": "Array of slot scores",
      "type": "array",
      "items": {
          "type": "integer"
      }
    }
  },
  "required": [
    "slot",
    "scores"
  ]
}

Assigns the given score to the item at the specified slot. If the item at the specified 
slot already had a score, that score is overwritten and any PerformanceEstimates for this 
slot or afterward are invalid and will be discarded.
Returns HTTP status 201 on success, or HTTP status 400 on failure.

The rule is that you cannot call selectNextItems again until all of your current items have been 
scored.  It doesn't matter if you pass the scores one at a time or in batches.  If you allow students 
to go back and change their answers, you can change scores as often as you want, in any order that you want.

I am a little nervous about skipping forward and scoring an item with previous items unscored, mostly 
because of resumes.  As currently implemented, replaceItems can only be called on the first unscored item.  
But if there's a scored item after that, it would be discarded along with everything else after the first 
unscored item violating the "once an item is scored, it cannot be replaced" rule.  We don't currently do 
this, and it's not a well-tested scenario.

--------------------------------------------------------------------
GET /data/{examId}/{studentId}/{strandLevel} - gets student test data.

String examId
String studentId
boolean strandLevel -  - if true, each line's performance estimate columns will 
contain strand-level values for the strand assigned to that line's slot. If false, 
each line will contain the "overall" estimate. 

Returns a human-readable representation of the current student state. The data 
will be formatted as a table, with one line per item assigned to the student, 
where each line is composed of whitespace-separated columns. 
The table returned will include lines ONLY for assigned items from the original inputText. 

--------------------------------------------------------------------
POST /data/{examId}/{strandLevel} - gets student test data.

String examId
boolean strandLevel -  - if true, each line's performance estimate columns will 
contain strand-level values for the strand assigned to that line's slot. If false, 
each line will contain the "overall" estimate. 

Request payload is JSON formatted StudentState object:
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "The StudentState",
  "type": "object",
  "properties": {
    "state": {
      "description": "Base64 encoded student state",
      "type": "string"
    },
    "reviewable": {
      "description": "Is student in reviewable range",
      "type": "boolean"
    }
  },
  "required": [
  	"studentState",
  	"reviewable"
  ]
}

Returns a human-readable representation of the current student state. The data 
will be formatted as a table, with one line per item assigned to the student, 
where each line is composed of whitespace-separated columns. 
The table returned will include lines ONLY for assigned items from the original inputText. 

--------------------------------------------------------------------
GET /estimate/{examId}/{studentId} - gets final performance estimate.

Returns an object map of final performance estimates for all strands in the test. Strands 
with no operational items assigned may be omitted from the returned map. The method 
returns null if no strands have performance estimates. 
The top level of the object map is a Map of strand names to strand performance estimates. 

The second level of the object map is a strand performance estimate Map of name/value pairs. 
Every strand-specific map is guaranteed to include a pair with a name of "model", and a 
value that indicates which PsychometricModel was used to calculate the performance estimate. 
The other pairs will be model-specific.

--------------------------------------------------------------------
POST /estimate/{examId} - gets final performance estimate.

Request payload is JSON formatted StudentState object:
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "The StudentState",
  "type": "object",
  "properties": {
    "state": {
      "description": "Base64 encoded student state",
      "type": "string"
    },
    "reviewable": {
      "description": "Is student in reviewable range",
      "type": "boolean"
    }
  },
  "required": [
  	"studentState",
  	"reviewable"
  ]
}

Returns an object map of final performance estimates for all strands in the test. Strands 
with no operational items assigned may be omitted from the returned map. The method 
returns null if no strands have performance estimates. 
The top level of the object map is a Map of strand names to strand performance estimates. 

The second level of the object map is a strand performance estimate Map of name/value pairs. 
Every strand-specific map is guaranteed to include a pair with a name of "model", and a 
value that indicates which PsychometricModel was used to calculate the performance estimate. 
The other pairs will be model-specific.

--------------------------------------------------------------------
DELETE /delete/{examId}/{studentId} - deletes student data

Use when student data has to be deleted.

********************************************************************************************
Set up SSL with Spring Boot
---------------------------------------

Obtain certificate and key files (BT wild-card certificate)
Copy certificate and key files to DCM server at /opt/dcm
Concatenate key and certificate file: cat btwildkey.key btwild.cer gd_bundle-g2-g1.crt>btwildkeycertificate.txt
Create a KeyStore in PKCS12 format: openssl pkcs12 -export -in btwildkeycertificate.txt -out dcmkeystore.pkcs12 -name btwild -noiter -nomaciter
Use password: dcm-keyst0re
Check iptables for open port 443.
Run server with: nohup java -jar /opt/dcm/cat-dcm-service-0.3.0.jar inputdir=/opt/dcm/int/input studentdir=/opt/dcm/int/student > ssl.out 2>&1&
********************************************************************************************
